package itis.quiz.spaceships;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class SpaceshipFleetManagerTestWithLib {

	SpaceshipFleetManager center = new CommandCenter();
	ArrayList<Spaceship> testList = new ArrayList<>();
	boolean flag = true;

	@AfterEach
	public void clearTestList(){
		testList.clear();
	}

	@Test
	@DisplayName("список мирных кораблей, возвращает не пустой список с мирными кораблями")
	public void getAllCivilianShips_peacefulShips_returnNotEmptyList() {
		int counter = 0;
		testList.add(new Spaceship("A", 0, 0, 0));
		testList.add(new Spaceship("A", 0, 0, 0));
		if (center.getAllCivilianShips(testList).size() == 0) {
			flag = false;
		}
		for (int i = 0; i < center.getAllCivilianShips(testList).size(); i++) {
			if (center.getAllCivilianShips(testList).get(i).getFirePower() != 0) {
				flag = false;
				break;
			}
		}
		Assertions.assertTrue(flag);
	}
	@Test
	@DisplayName("список мирных кораблей, возвращает пустой список")
	public void getAllCivilianShips_peacefulShips_returnEmptyList() {
		testList.add(new Spaceship("A", 1, 0, 0));
		testList.add(new Spaceship("A", 1, 0, 0));
		testList.add(new Spaceship("A", 1, 0, 0));
		Assertions.assertEquals(0,center.getAllCivilianShips(testList).size());
	}
	@Test
	@DisplayName("самый мощный корабль, возвращает самый мощный корабль")
	public void getMostPowerfulShip_mostPowerfulShip_returnMostPowerfulShip() {
		int testPower = 10;
		testList.add(new Spaceship("A", 1, 0, 0));
		testList.add(new Spaceship("A", 2, 0, 0));
		testList.add(new Spaceship("D", 3, 0, 0));
		testList.add(new Spaceship("B", testPower, 0, 0));
		Spaceship testMostPowerfulShip = center.getMostPowerfulShip(testList);
		boolean testCondition = testMostPowerfulShip.getFirePower() == testPower && testMostPowerfulShip.getFirePower() > 0;
		Assertions.assertTrue(testCondition);
	}
	@Test
	@DisplayName("самый мощный корабль, возвращает именно ПЕРВЫЙ самый мощный по списку")
	public void getMostPowerfulShip_mostPowerfulShip_returnFirstInListMostPowerfulShip() {
		int testPower = 10;
		String testName = "truly powerful ship";
		testList.clear();
		testList.add(new Spaceship("A", 1, 0, 0));
		testList.add(new Spaceship("A", 2, 0, 0));
		testList.add(new Spaceship(testName, testPower, 0, 0));
		testList.add(new Spaceship("B", testPower, 0, 0));
		Spaceship testMostPowerfulShip = center.getMostPowerfulShip(testList);
		boolean testCondition = testMostPowerfulShip.getFirePower() == testPower && testMostPowerfulShip.getName().equals(testName)
				&& testMostPowerfulShip.getFirePower() > 0;
		Assertions.assertTrue(testCondition);
	}
	@Test
	@DisplayName("самый мощный корабль, возвращается null")
	public void getMostPowerfulShip_mostPowerfulShip_returnNull() {
		testList.add(new Spaceship("A", 0, 0, 0));
		testList.add(new Spaceship("A", -1, 0, 0));
		testList.add(new Spaceship("A", 0, 0, 0));
		Spaceship testMostPowerfulShip = center.getMostPowerfulShip(testList);
		Assertions.assertNull(testMostPowerfulShip);
	}
	@Test
	@DisplayName("корабль с заданным именем, возвращается правильный")
	public void getShipByName_shipName_returnShipWithCorrectName() {
		testList.add(new Spaceship("Big", 1, 1, 1));
		testList.add(new Spaceship("Small", 1, 1, 1));
		testList.add(new Spaceship("Mid", 1, 1, 1));
		String name = "Mid";
		Spaceship shipByName = center.getShipByName(testList, name);
		if (shipByName != null && shipByName.getName().equals(name)) {
			flag = true;
		}
		Assertions.assertTrue(flag);
	}
	@Test
	@DisplayName("корабль с заданным именем, возвращается null")
	public void getShipByName_shipName_returnNull() {
		testList.add(new Spaceship("Big", 1, 1, 1));
		testList.add(new Spaceship("Small", 1, 1, 1));
		testList.add(new Spaceship("Mid", 1, 1, 1));
		String name = "MidRak";
		Spaceship shipByName = center.getShipByName(testList, name);
		Assertions.assertNull(shipByName);
	}
	@Test
	@DisplayName("корабли с подходящим трюмом, возвращается список с удовлетворяющими условию кораблями")
	public void getAllShipsWithEnoughCargoSpace_shipListWithEnoughCargoSpace_returnListWithShips(){
		testList.add(new Spaceship("Big", 1, 1, 1));
		testList.add(new Spaceship("Small", 1, 10, 1));
		testList.add(new Spaceship("Mid", 1, 11, 1));
		int testCargoSpace = 10;
		ArrayList<Spaceship> testSpaceCargoShips = center.getAllShipsWithEnoughCargoSpace(testList,testCargoSpace);
		if(testSpaceCargoShips.size() == 0){
			flag = false;
		}
		for (Spaceship testSpaceCargoShip : testSpaceCargoShips) {
			if (testSpaceCargoShip.getCargoSpace() < testCargoSpace) {
				flag = false;
			}
		}
		Assertions.assertTrue(flag);
	}
	@Test
	@DisplayName("корабли с подходящим трюмом, возвращается пустой список")
	public void getAllShipsWithEnoughCargoSpace_shipListWithEnoughCargoSpace_returnEmptyList(){
		testList.add(new Spaceship("Big", 1, 1, 1));
		testList.add(new Spaceship("Small", 1, 9, 1));
		testList.add(new Spaceship("Mid", 1, 9, 1));
		int testCargoSpace = 10;
		ArrayList<Spaceship> testSpaceCargoShips = center.getAllShipsWithEnoughCargoSpace(testList,testCargoSpace);
		Assertions.assertEquals(0,testSpaceCargoShips.size());
	}


}
