package itis.quiz.spaceships;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {

	SpaceshipFleetManager center;
	ArrayList<Spaceship> testList = new ArrayList<>();

	public static void main(String[] args) {

		SpaceshipFleetManagerTest spaceshipFleetManager = new SpaceshipFleetManagerTest(new CommandCenter());
		boolean test1 = spaceshipFleetManager.getAllCivilianShips_peacefulShips_returnNotEmptyList();
		boolean test2 = spaceshipFleetManager.getAllCivilianShips_peacefulShips_returnEmptyList();
		boolean test3 = spaceshipFleetManager.getAllShipsWithEnoughCargoSpace_shipListWithEnoughCargoSpace_returnEmptyList();
		boolean test4 = spaceshipFleetManager.getAllShipsWithEnoughCargoSpace_shipListWithEnoughCargoSpace_returnListWithShips();
		boolean test5 = spaceshipFleetManager.getMostPowerfulShip_mostPowerfulShip_returnFirstInListMostPowerfulShip();
		boolean test6 = spaceshipFleetManager.getMostPowerfulShip_mostPowerfulShip_returnMostPowerfulShip();
		boolean test7 = spaceshipFleetManager.getMostPowerfulShip_mostPowerfulShip_returnNull();
		boolean test8 = spaceshipFleetManager.getShipByName_shipName_returnNull();
		boolean test9 = spaceshipFleetManager.getShipByName_shipName_returnShipWithCorrectName();
		ArrayList<Boolean> tests = new ArrayList<>();
		tests.add(test1);
		tests.add(test2);
		tests.add(test3);
		tests.add(test4);
		tests.add(test5);
		tests.add(test6);
		tests.add(test7);
		tests.add(test8);
		tests.add(test9);
		spaceshipFleetManager.printBalls(tests);
	}

	private boolean getAllCivilianShips_peacefulShips_returnNotEmptyList() {
		testList.clear();
		testList.add(new Spaceship("A", 0, 0, 0));
		testList.add(new Spaceship("A", 1, 0, 0));
		testList.add(new Spaceship("A", 1, 0, 0));
		if (center.getAllCivilianShips(testList).size() == 0) {
			return false;
		}
		for (int i = 0; i < center.getAllCivilianShips(testList).size(); i++) {
			if (center.getAllCivilianShips(testList).get(i).getFirePower() != 0) {
				testList.clear();
				return false;
			}
		}
		testList.clear();
		return true;
	}

	private boolean getAllCivilianShips_peacefulShips_returnEmptyList() {
		testList.clear();
		testList.add(new Spaceship("A", 1, 0, 0));
		testList.add(new Spaceship("A", 1, 0, 0));
		testList.add(new Spaceship("A", 1, 0, 0));
		if (center.getAllCivilianShips(testList).size() == 0) {
			testList.clear();
			return true;
		}
		testList.clear();
		return false;
	}

	private boolean getMostPowerfulShip_mostPowerfulShip_returnMostPowerfulShip() {
		int testPower = 10;
		testList.clear();
		testList.add(new Spaceship("A", 1, 0, 0));
		testList.add(new Spaceship("A", 2, 0, 0));
		testList.add(new Spaceship("D", 3, 0, 0));
		testList.add(new Spaceship("B", testPower, 0, 0));
		Spaceship testMostPowerfulShip = center.getMostPowerfulShip(testList);
		if (testMostPowerfulShip.getFirePower() == testPower && testMostPowerfulShip.getFirePower() > 0) {
			testList.clear();
			return true;
		}
		return false;
	}

	private boolean getMostPowerfulShip_mostPowerfulShip_returnFirstInListMostPowerfulShip() {
		int testPower = 10;
		String testName = "truly powerful ship";
		testList.clear();
		testList.add(new Spaceship("A", 1, 0, 0));
		testList.add(new Spaceship("A", 2, 0, 0));
		testList.add(new Spaceship(testName, testPower, 0, 0));
		testList.add(new Spaceship("B", testPower, 0, 0));
		Spaceship testMostPowerfulShip = center.getMostPowerfulShip(testList);
		if (testMostPowerfulShip.getFirePower() == testPower && testMostPowerfulShip.getName().equals(testName) && testMostPowerfulShip.getFirePower() > 0) {
			testList.clear();
			return true;
		}
		return false;
	}

	private boolean getMostPowerfulShip_mostPowerfulShip_returnNull() {
		testList.clear();
		testList.add(new Spaceship("A", 0, 0, 0));
		testList.add(new Spaceship("A", -1, 0, 0));
		testList.add(new Spaceship("A", 0, 0, 0));
		Spaceship testMostPowerfulShip = center.getMostPowerfulShip(testList);
		if (testMostPowerfulShip == null) {
			testList.clear();
			return true;
		}
		return false;
	}

	private boolean getShipByName_shipName_returnShipWithCorrectName() {
		testList.clear();
		testList.add(new Spaceship("Big", 1, 1, 1));
		testList.add(new Spaceship("Small", 1, 1, 1));
		testList.add(new Spaceship("Mid", 1, 1, 1));
		String name = "Mid";
		Spaceship shipByName = center.getShipByName(testList, name);
		if (shipByName != null && shipByName.getName().equals(name)) {
			testList.clear();
			return true;
		}
		testList.clear();
		return false;
	}

	private boolean getShipByName_shipName_returnNull() {
		testList.clear();
		testList.add(new Spaceship("Big", 1, 1, 1));
		testList.add(new Spaceship("Small", 1, 1, 1));
		testList.add(new Spaceship("Mid", 1, 1, 1));
		String name = "MidRak";
		Spaceship shipByName = center.getShipByName(testList, name);
		if (shipByName == null) {
			testList.clear();
			return true;
		}
		testList.clear();
		return false;
	}

	private boolean getAllShipsWithEnoughCargoSpace_shipListWithEnoughCargoSpace_returnListWithShips(){
		testList.clear();
		testList.add(new Spaceship("Big", 1, 1, 1));
		testList.add(new Spaceship("Small", 1, 10, 1));
		testList.add(new Spaceship("Mid", 1, 11, 1));
		int testCargoSpace = 10;
		ArrayList<Spaceship> testSpaceCargoShips = center.getAllShipsWithEnoughCargoSpace(testList,testCargoSpace);
		if(testSpaceCargoShips.size() == 0){
			testList.clear();
			return false;
		}
		for (Spaceship testSpaceCargoShip : testSpaceCargoShips) {
			if (testSpaceCargoShip.getCargoSpace() < testCargoSpace) {
				testList.clear();
				return false;
			}
		}
		testList.clear();
		return true;
	}

	private boolean getAllShipsWithEnoughCargoSpace_shipListWithEnoughCargoSpace_returnEmptyList(){
		testList.clear();
		testList.add(new Spaceship("Big", 1, 1, 1));
		testList.add(new Spaceship("Small", 1, 9, 1));
		testList.add(new Spaceship("Mid", 1, 9, 1));
		int testCargoSpace = 10;
		ArrayList<Spaceship> testSpaceCargoShips = center.getAllShipsWithEnoughCargoSpace(testList,testCargoSpace);
		if(testSpaceCargoShips.size() == 0){
			testList.clear();
			return true;
		}
		testList.clear();
		return false;
	}

	private void printBalls(ArrayList<Boolean> testsList){
		double balls = 0.0;
		for (int i = 0; i < testsList.size() ; i++) {
			if(testsList.get(i)){
				System.out.println("Test " + i + " is done");
				balls += 0.45;
			}
			else {
				System.out.println("Test " + i + " is not done ");
			}
		}
		String formattedBalls = String.format("%.2f",balls);
		System.out.println("Your balls is " + formattedBalls);
	}

	public SpaceshipFleetManagerTest(SpaceshipFleetManager center) {
		this.center = center;
	}
}
