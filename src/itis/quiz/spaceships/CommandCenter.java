package itis.quiz.spaceships;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {
	@Override
	public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
		Spaceship superPowered = new Spaceship("", 0, 0, 0);
		superPowered.setFirePower(0);
		for (int i = 0; i < ships.size(); i++) {
			if (ships.get(i).getFirePower() > superPowered.getFirePower()) {
				superPowered = ships.get(i);
			}
		}
		if (superPowered.getFirePower() > 0) {
			return superPowered;
		}
		return null;
	}

	public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
		for (int i = 0; i < ships.size(); i++) {
			if (ships.get(i).getName().equals(name)) {
				return ships.get(i);
			}
		}
		return null;
	}

	@Override
	public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
		ArrayList<Spaceship> bigSpaceship = new ArrayList<>();
		for (int i = 0; i < ships.size(); i++) {
			if (ships.get(i).getCargoSpace() >= cargoSize) {
				bigSpaceship.add(ships.get(i));
			}
		}
		return bigSpaceship;
	}


	@Override
	public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
		ArrayList<Spaceship> spaceships = new ArrayList<>();
		for (int i = 0; i < ships.size(); i++) {
			if (ships.get(i).getFirePower() <= 0) {
				spaceships.add(ships.get(i));
			}
			if (spaceships.size() != 0) {
				return spaceships;
			}
		}
		return spaceships;
	}
}
